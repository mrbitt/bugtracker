package com.causecode.models;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.HashMap;

public class MonitorUnassigned implements Runnable{
	/*
	 * runs infinitely
	 * Hourly checks for those bugs which remain unassigned due to
	 * no free bug solvers
	 * Assign if any bug solver gets free
	 */
	public void run() {
		try{
			Class.forName("com.mysql.jdbc.Driver").newInstance(); 
			Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/bugtracker","root"," ");
			HashMap<String,String> map = new HashMap<String, String>();
			Statement st = conn.createStatement();
			AssignBugs ab = new AssignBugs();
			String command = "select bugId,category from bug where assignedto='nobody'";
			st.execute(command);
			ResultSet set = st.getResultSet();
				while(set.next()){
					map.put(set.getString(1), set.getString(2));
				}
			if(map.size()>0){
				for(String s:map.keySet()){
					ab.assign(s, map.get(s));
				}
			}
			Thread.sleep(3600000);
		}catch(Exception e){
			e.printStackTrace();
		}
		
	}
}

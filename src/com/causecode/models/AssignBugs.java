package com.causecode.models;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

public class AssignBugs {
	public boolean assign(String bugId,String category){
		boolean ok=false;
		//connecting to database
		Connection conn=null;
		try{
			Class.forName("com.mysql.jdbc.Driver").newInstance(); 
			conn=DriverManager.getConnection("jdbc:mysql://localhost:3306/bugtracker","root"," ");
		}catch(Exception e){
			System.out.println("problem establishing connection with database...");
		}
		
		ArrayList<String> freeSolvers = new ArrayList<String>();
		try{
			String command = "select bsid from bug_solver where category="+"\""+category+"\""+" AND isfree=true";
			Statement st = conn.createStatement();
			st.execute(command);
			ResultSet rs = st.getResultSet();
				while(rs.next()){
					freeSolvers.add(rs.getString(1));
				}
			int len = freeSolvers.size();
			
			
			if(len>0){
				//updating bug table
				int randomSelect = ((int)(Math.random()*((int)(Math.log10(len)*10))))%len;
				String selectedSolver = freeSolvers.get(randomSelect);
				command = "update bug "+
								"set assignedto = "+"\""+selectedSolver+"\""+" "+
								"where bugid = "+"\""+bugId+"\"";
				st = conn.createStatement();
				st.executeUpdate(command);
				ok=true;
				
				//writing to timeline
				try{
					st = conn.createStatement();
					st.execute("select name from bug_solver where bsid="+"\""+selectedSolver+"\"");
					rs = st.getResultSet();
					String name="";
					if(rs.next()){
							name = rs.getString(1);
					}
					
					st = conn.createStatement();
					st.execute("update bug_solver set isfree=false where bsid="+"\""+selectedSolver+"\"");
						
							BufferedWriter writer = new BufferedWriter(new FileWriter("timeline.txt",true));
							writer.append("Bug("+bugId+") is assigned to solver "+name+"("+selectedSolver+")");
							writer.newLine();
							writer.close();
				}catch(Exception e){
					e.printStackTrace();
				}
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return ok;
	}
}

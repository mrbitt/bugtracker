package com.causecode.controllers;

public class AsObject {
	String name;
	String mailId;
	String password;
	String bugDesc;
	String bugId;
	String bugSeverity;
		AsObject(String n,String m,String p,String bi,String bs,String bd){
			name=n;
			mailId=m;
			password=p;
			bugDesc=bd;
			bugId=bi;
			bugSeverity=bs;
		}
		public String getName(){
			return name;
		}
		public String getMail(){
			return mailId;
		}
		public String getPass(){
			return password;
		}
		
		public String getDesc(){
			return bugDesc;
		}
		public String getId(){
			return bugId;
		}
		public String getSeverity(){
			return bugSeverity;
		}
}

package com.causecode.controllers;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class AdminLoginHandler extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String name=request.getParameter("uname");
		String password = request.getParameter("password");
		RequestDispatcher rd;	
		if(name.equals("admin") && password.equals("admin")){
				request.setAttribute("admin_entry", "yes");
				rd = request.getRequestDispatcher("admin_timeline.jsp");
				rd.forward(request, response);
			}else{
				request.setAttribute("admin_entry", "no");
				request.setAttribute("error","wrong credentials!!");
				rd = request.getRequestDispatcher("admin_login.jsp");
				rd.forward(request, response);
			}
	}

}

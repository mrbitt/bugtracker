package com.causecode.controllers;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.causecode.models.AssignBugs;
import com.causecode.models.MonitorUnassigned;

public class BugHandler extends HttpServlet {
	private static final long serialVersionUID = 1L;
	static int idNumber=1;
	
	public void init() throws ServletException {
		super.init();
		Thread thread = new Thread(new MonitorUnassigned());
		thread.start();
			try{
				Class.forName("com.mysql.jdbc.Driver").newInstance(); 
				getServletConfig().getServletContext().setAttribute("databaseConnection",DriverManager.getConnection("jdbc:mysql://localhost:3306/bugtracker","root"," "));
			}catch(Exception e){
				System.out.println("problem establishing connection with database...");
			}
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		Date d = new Date();
		Connection conn = (Connection) getServletConfig().getServletContext().getAttribute("databaseConnection");
		
		//Bug parameters
		String category = request.getParameter("category");
		String severity = request.getParameter("severity");
		String summary = request.getParameter("description");
		String mailId = request.getParameter("mailId");
		boolean annonymous = mailId.trim().equals("")?true:false;
		String status = "new";
		String assignedTo = "nobody";
		String dateAndTime = dateFormat.format(d);
		String bugId="";
		try{
			Statement st = conn.createStatement();
			st.execute("select count(*) from bug");
			ResultSet rs = st.getResultSet();
			int val=0;
			if(rs.next())
				val = Integer.parseInt(rs.getString(1));
			bugId = "CC-BUG-"+(val+1);
		}catch(Exception e){
			System.out.println("Error getting values");
		}
		
		//adding value to database
		try{
			String command = "insert into bug values("+"\""+bugId+"\""+","+"\""+category+"\""+","+"\""+severity+"\""+","+"\""+status+"\""+	","+"\""+summary.trim()+"\""+","+"\""+dateAndTime+"\""+","+annonymous+","+"\""+mailId+"\""+","+"\""+assignedTo+"\""+")";
			Statement st = conn.createStatement();
			st.executeUpdate(command);
				BufferedWriter writer = new BufferedWriter(new FileWriter("timeline.txt",true));
				writer.append("New bug("+bugId+") fired at "+dateAndTime);
				writer.newLine();
				writer.close();
			AssignBugs ab = new AssignBugs();
			ab.assign(bugId,category);
			//later use
			

		}catch(Exception e){
			System.out.println("error storing bug information");
		}
		
		response.getWriter().println("<div align=\"center\"><b>Thanks</b></div>");
	}

}

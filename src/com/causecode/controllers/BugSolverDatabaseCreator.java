package com.causecode.controllers;

import java.io.BufferedReader;
import java.io.FileReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;

public class BugSolverDatabaseCreator {
	public static void main(String[] args){
		BufferedReader reader;
		int count=1;
		String[] categories = {"upgrade","authentication","performence","user interface","plug-ins"};
		String[] mailEx = {"gmail.com","yahoo.com","outlook.com"};
		String[] prefix = {"cause","code","causecode","codecause"};
		Connection conn=null;	
			try{
				Class.forName("com.mysql.jdbc.Driver").newInstance();
				conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/bugtracker","root"," ");
			}catch(Exception e){
				System.out.println("unable to establish connection with database");
			}
			try{
				reader = new BufferedReader(new FileReader("bugSolverNames.txt"));
				String name="";
				String category="";
				String mailId="";
				String password="";
				String bsId="";
				
					while((name=reader.readLine())!=null){
						bsId = "CC-BS-"+(count++);
						category = categories[((int)(Math.random()*10))%5];
						mailId=name+""+(int)(Math.random()*100)+""+"@"+mailEx[((int)(Math.random()*10))%3];
						password = (name+"@"+category+""+prefix[((int)(Math.random()*10))%4]);
						
						password=password.replaceAll(" ","");
						mailId = mailId.replaceAll(" ","");
						if(password.length()>40)
							{
								password=password.substring(0,40);
							}
						
						
						//adding to database
						Statement st = conn.createStatement();
						String command = "insert into bug_solver values("+"\""+bsId+"\""+","+"\""+name+"\""+","+"\""+category+"\""+","+(true)+","+"\""+mailId+"\""+","+"\""+password+"\""+")";
						st.executeUpdate(command);
					}
			}catch(Exception e){
				e.printStackTrace();
			}
	}
}

package com.causecode.controllers;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class AfterBugSolvedHandler extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String bugId=request.getParameter("bugId");
		String name=request.getParameter("name");
		String mailId=request.getParameter("mail");
		String password=request.getParameter("pass");
		
		//adding to timeline
		try{
			BufferedWriter writer = new BufferedWriter(new FileWriter("timeline.txt"));
			writer.append(name+" solved bug("+bugId+")");
			writer.newLine();
			writer.close();
		}catch(Exception e){
			e.printStackTrace();
		}
		
		//changing bug and bug_solver table
		try{
			//bug is now deleted from table
			Class.forName("com.mysql.jdbc.Driver").newInstance(); 
			Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/bugtracker","root"," ");
			Statement st = conn.createStatement();
			String command = "delete from bug "+
							 "where bugId="+"\""+bugId+"\"";
			st.execute(command);
			
			//bug solver is now free
			st = conn.createStatement();
			command = "update bug_solver "+
					  "set isFree=true where name="+"\""+name+"\""+
					  " AND mailId="+"\""+mailId+"\""+
					  " AND password="+"\""+password+"\"";
			st.execute(command);
		}catch(Exception e){
			e.printStackTrace();
		}
		
		response.getWriter().println("<div align=\"center\"><b>Thanks</b></div>");
	}

}

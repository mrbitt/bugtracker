package com.causecode.controllers;


import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class BugSolverLoginHandler extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static Connection conn;
	public void init() throws ServletException {
		super.init();
		
		try{
				Class.forName("com.mysql.jdbc.Driver").newInstance(); 
				getServletConfig().getServletContext().setAttribute("databaseConnection",DriverManager.getConnection("jdbc:mysql://localhost:3306/bugtracker","root"," "));
			}catch(Exception e){
				System.out.println("problem establishing connection with database...");
			}
	}
	private boolean isThere(String name,String mail,String password){
		conn = (Connection) getServletConfig().getServletContext().getAttribute("databaseConnection");
		boolean ok=false;
		String command = "select count(*) from bug_solver "+
				 "where name = ?"+
				 "AND mailId = ?"+
				 "AND password = ?";
		try{
			PreparedStatement st = conn.prepareStatement(command);
				st.setString(1, name);
				st.setString(2, mail);
				st.setString(3, password);
				
			st.execute();
			ResultSet rs = st.getResultSet();
			int val=0;
				if(rs.next()){
					val = Integer.parseInt(rs.getString(1));
				}
				if(val==1){
					ok=true;
				}
		}catch(Exception e){
			//do nothing
		}
		return ok;	
	}
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String name = request.getParameter("uname");
		String password = request.getParameter("password");
		String mailId = request.getParameter("mailId");

			if(isThere(name,mailId,password)){
				String bugSolverId="";
				try{
					String command="select bsid from bug_solver where "+
								   "name = ?"+
								   "AND password = ?"+
								   "AND mailId = ?";
					
					PreparedStatement st = conn.prepareStatement(command);
					st.setString(1, name);
					st.setString(2, password);
					st.setString(3, mailId);
					st.execute();
					
					ResultSet set = st.getResultSet();
						if(set.next())
							bugSolverId = set.getString(1);
						
					Statement s = conn.createStatement();
					command = "select bugid,severity,summary from bug where assignedto="+"\""+bugSolverId+"\"";
					s.execute(command);
					
					set = s.getResultSet();
						if(set.next()){
							AsObject ao = new AsObject(name,mailId,password,set.getString(1), set.getString(2), set.getString(3));
							request.setAttribute("solver_entry", "yes");
							request.setAttribute("solverObject",ao);
							RequestDispatcher rd = request.getRequestDispatcher("solverPanel.jsp");
							rd.forward(request, response);
						}
				}catch(Exception e){
					e.printStackTrace();
				}
				
			}else{
				request.setAttribute("solver_entry", "no");
				request.setAttribute("error","wrong credentials!!");
				RequestDispatcher rd = request.getRequestDispatcher("bug_solver_login.jsp");
				rd.forward(request, response);
			}
		
	}

}

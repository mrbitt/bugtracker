<%@page import="java.util.Stack"%>
<%@page import="java.io.FileReader"%>
<%@page import="java.io.BufferedReader"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>timeline</title>
</head>
<%
	if(request.getAttribute("admin_entry")==null || request.getAttribute("admin_entry").equals("no")){
		response.sendRedirect("admin_login.jsp");
	}
%>
<body>
<div style="border: 5px double blue;">
	<%
		try{
			BufferedReader reader = new BufferedReader(new FileReader("timeline.txt"));
			String line="";
			Stack<String> stack = new Stack<String>();
				while((line=reader.readLine())!=null){
					stack.push(line);
				}
			reader.close();
				for(String s:stack){
					out.println("<font size=3>"+s+"</font>");
					out.println("<br>");
				}
		}catch(Exception e){
			e.printStackTrace();
		}
	%>
</div>
</body>
</html>
<%@page import="com.causecode.controllers.AsObject"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Solver Panel</title>
</head>
<%
	if(request.getAttribute("solver_entry")==null || request.getAttribute("solver_entry").equals("no")){
		response.sendRedirect("bug_solver_login.jsp");
	}
%>
<body>
	<%  AsObject ao = (AsObject) request.getAttribute("solverObject"); %>
	<div align="right">
		<font color="blue"><b>HELLO <%=ao.getName()%></b></font><br>
		<font color="blue"><b><%=ao.getMail()%></b></font><br>
		</div>
	<b>Bug ID:</b><%=ao.getId() %><br>
	<b>Bug Severity:</b><%=ao.getSeverity() %><br>
	<b>Bug Description:</b><%=ao.getDesc() %><br>
	
	<div style="border : 5px double blue;">
	<form action="causecode.bug-solved" method="post">
		<input type="checkbox" name="analysed">Analysis is done<br>
		<input type="checkbox" name="started_work">Started working on it<br>
		<input type="checkbox" name="ended_work">Work is done<br>
		<input type="checkbox" name="dry_run">Dry run is done<br>
		<input type="checkbox" name="ready_to_merge">Ready to merge<br>
		<input type="checkbox" name="i_m_free">I am free<br>
		<input type="hidden" name="bugId" value="<%=ao.getId()%>"/>
		<input type="hidden" name="name" value="<%=ao.getName()%>"/>
		<input type="hidden" name="mail" value="<%=ao.getMail()%>"/>
		<input type="hidden" name="pass" value="<%=ao.getPass()%>"/>
		
		<input type="submit" value="proceed"/>
	</form>
	</div>
</body>
</html>
Hello sir,

Here is the procedure to use this web app.

1.Create a database of random bug solvers by running "BugSolverDatabaseCreator.java"
2.Report a bug from "report_bug.html".This will update "bug" database.
3.To login as admin, open "admin_login.jsp" with username="admin" password="admin"
4.Bug is randomly assigned to a bug solver of that category who is free at that time.
5.To know who is engaged in work,check timeline as admin
6.To login as bug solver, open "bug_solver_login.jsp" with username,mailId,password copied from database "bug_solver"
7.Bug solvers are given solver panel where they can work.
8.This app hourly checks for those bugs which remains unassigned due to no free bug solvers at the time.